import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Created by ilyushchenko on 09.01.2017.
 */
public class PageParser {

    public static void main(String[] args) {
        try {
            LocalDate curDate = LocalDate.of(2017, 5, 31);
            LocalDate endDate = LocalDate.of(2017, 5, 30);
            while (curDate.isAfter(endDate)) {
                try {
                    parseDay(curDate);
                } catch (Exception e) {
                    System.out.println("Ошибка при парсинге " + curDate.format(DateTimeFormatter.ofPattern("dd-MMM-uuuu", Locale.ENGLISH)));
                    e.printStackTrace();
                }
                curDate = curDate.minusDays(1);
            }
        } catch (Exception mue) {
            mue.printStackTrace();
        }
    }

    private static void parseDay(LocalDate day) throws Exception {
        Document docDay = getDoc("http://www.attheraces.com/racecards/"+day.format(DateTimeFormatter.ofPattern("dd-MMM-uuuu", Locale.ENGLISH)));
        Elements panels = docDay.select(".panel.uk");
        for (int i = 0; i < panels.size(); i++) {
            Element panel = panels.get(i);
            String weather = null;
            Elements weatherSpans = panel.select(".meeting-information").select("span");
            for (Element span : weatherSpans) {
                if (span.text().contains("Weather")) {
                    weather = span.text().split(":")[1].substring(1);
                }
            }
            if (weather==null) {
                return;
            }
            System.out.println(weather);
            Elements races = panel.select(".finished.uk");
            for (int j=0; j<races.size(); j++) {
                try {
                    Element element = races.get(j);
//                    System.out.println(element.select("a.meeting").attr("href"));
                    parseRace("http://www.attheraces.com"+element.select("a.meeting").attr("href"), weather);
                } catch (Exception e) {
                    System.out.println("Ошибка при парсинге гонки");
                    e.printStackTrace();
                }
            }
        }

    }

    private static void parseRace(String href, String weather) throws Exception {
        String[] raceData = new String[6];
        Document race = getDoc(href);
//        Integer raceId = Integer.parseInt(race.html().substring(race.html().indexOf("RaceID:")+7, race.html().indexOf("PaRaceID")-10));
        Integer distance = getYards(race.select(".distance").select("span").text());
        LocalDate raceDate = null;
        try {
            raceDate = LocalDate.parse(race.select("span.date").text(), DateTimeFormatter.ofPattern("dd MMM uuuu", Locale.ENGLISH));
        } catch (Exception e) {
            raceDate = LocalDate.parse(race.select("span.date").text(), DateTimeFormatter.ofPattern("d MMM uuuu", Locale.ENGLISH));
        }
        String going = race.select(".race-head__distance-and-going").select("p").get(1).text();
        Integer winningTime = getMilliseconds(race.select(".racecard-results-footer.panel-footer.panel-inner").select("p").get(1).text().split(":")[1].substring(1));
        Elements rows = race.select(".table-racecard-results tr:not(.non-runner)");
        Integer horseTime = winningTime;
        for (int i = 1; i < rows.size(); i++) {
            Element horse = rows.get(i);
            Document horsePage = getDoc("http://www.attheraces.com"+horse.select(".name.summary.form-link.horse-form-link").attr("href"));
            System.out.println("http://www.attheraces.com"+horse.select(".name.summary.form-link.horse-form-link").attr("href"));
            Elements hRows = horsePage.select("#horse-form-full").select("tr:not(.divide.hidden-with-filters)");
            int k = 0;
            for (int j = 1; j < hRows.size(); j++) {
                Element row = hRows.get(j);
                String[] strDateMas = row.select("td a").get(0).text().split(" ");
                String strDate = strDateMas[0] + " " + strDateMas[1] + " " + strDateMas[2];
                LocalDate date = LocalDate.parse(strDate, DateTimeFormatter.ofPattern("dd MMM uu", Locale.ENGLISH));
                if (date.equals(raceDate)) {
                    k = j;
                }
            }
            if ((hRows.size()-k)<7) {
                continue;
            }

            String[][] horseRacesMas = new String[7][];
            int p = 0;
            for (int j = k; j < k+7; j++) {
                Element row = hRows.get(j);

                String[] strDateMas = row.select("td a").get(0).text().split(" ");
                Integer daysSinceLastRide = 0;
                if (strDateMas.length==4) {
                    daysSinceLastRide = Integer.valueOf(strDateMas[3].substring(1, strDateMas[3].length()-1));
                }
                String raceClass = row.attr("data-raceclass");
                if (raceClass.length()==0) {
                    break;
                }
                String raceType = row.select("td").get(1).select("a").get(3).attr("data-tooltip");
                String positionStr = row.select("td").get(3).select("span").get(0).text().split("/")[0];
                Integer position = null;
                if (positionStr.equals("Won")) {
                    position = 1;
                } else {
                    try {
                        position = Integer.valueOf(positionStr);
                    } catch (Exception e) {
                        break;
                    }
                }
//                String horseRaceHref = row.;
                System.out.println(positionStr + " " + position);
            }

            Integer position = i;
            Integer horseId = Integer.parseInt(horse.select(".horse-tracker-link").attr("data-horseid"));
            String horseName = horse.attr("data-horsename");
            Integer age = Integer.parseInt(horse.select("span.age").text());
            String[] weightMas = horse.select("span.weight").text().split("-");
            Integer weight = Integer.parseInt(weightMas[0])*14 + Integer.parseInt(weightMas[1].split(" ")[0]);
            String ratingStr = horse.select(".official-rating").select("span").get(0).text();
            Integer officialRating = ratingStr.equals("-")?null:Integer.parseInt(ratingStr);
            String breakAway = horse.select("td.distance").text();
            horseTime += parseBreakAway(breakAway);
        }
    }

    private static Document getDoc(String url) throws Exception {
        int tries = 5;
        while (tries>0) {
            try {
                return Jsoup.connect(url).userAgent("Chrome").get();
            } catch (Exception e) {
                tries--;
                if (tries==0) {
                    throw e;
                }
            }
        }
        return null;
    }

    private static Integer getYards(String distance) {
        Integer yards = 0;
        String[] distMas = distance.split(" ");
        for (String dist : distMas) {
            if (dist.contains("y")) {
                yards += Integer.parseInt(dist.replace("y",""));
            }
            if (dist.contains("f")) {
                yards += Integer.parseInt(dist.replace("f","")) * 220;
            }
            if (dist.contains("m")) {
                yards += Integer.parseInt(dist.replace("m","")) * 8 * 220;
            }
        }
        return yards;
    }

    private static Integer getMilliseconds(String winningTime) {
        Integer milliSeconds = 0;
        String[] timeMas = winningTime.split(" ");
        for (String time : timeMas) {
            if (time.contains("m")) {
                milliSeconds += Integer.parseInt(time.replace("m","")) * 60 * 1000;
            }
            if (time.contains("s")) {
                String[] secondsMas = time.replace("s","").split("\\.");
                milliSeconds += Integer.parseInt(secondsMas[0].length()>0?secondsMas[0]:"0") * 1000 + Integer.parseInt(secondsMas[1]) * 10;
            }
        }
        return milliSeconds;
    }

    private static Integer parseBreakAway(String breakAway) {
        if (breakAway.length()==0) {
            return 0;
        }
        Integer milliSeconds = 0;
        switch (breakAway) {
            case "shd": {
                milliSeconds += 1;
                break;
            }
            case "nse": {
                milliSeconds += 5;
                break;
            }
            case "hd": {
                milliSeconds += 15;
                break;
            }
            case "nk": {
                milliSeconds += 35;
                break;
            }
            default: {
                if (breakAway.contains("¼")) {
                    breakAway = breakAway.replace("¼","");
                    milliSeconds += 45;
                }
                if (breakAway.contains("½")) {
                    breakAway = breakAway.replace("½","");
                    milliSeconds += 90;
                }
                if (breakAway.contains("¾")) {
                    breakAway = breakAway.replace("¾","");
                    milliSeconds += 135;
                }
                if (breakAway.length()>0) {
                    milliSeconds += Integer.parseInt(breakAway) * 180;
                }
            }
        }
        return milliSeconds;
    }
}
