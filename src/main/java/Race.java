/**
 * Created by Alexey on 28.08.2017.
 */
public class Race {
    private int daysFromRace;
    private String raceType;
    private String hrefRace;
    private int place;
    private int distance;
    private String dateRace;
    private String groundType;
    private int countHorses;
    private int timeHorse;
    private String spHorse;
    private int idHorse;
    private String nameHorse;
    private int ageHorse;
    private int weightHorse;
    private int ratingHorse;

    public Race() {
    }

    public int getDaysFromRace() {

        return daysFromRace;
    }

    public void setDaysFromRace(int daysFromRace) {
        this.daysFromRace = daysFromRace;
    }

    public String getRaceType() {
        return raceType;
    }

    public void setRaceType(String raceType) {
        this.raceType = raceType;
    }

    public String getHrefRace() {
        return hrefRace;
    }

    public void setHrefRace(String hrefRace) {
        this.hrefRace = hrefRace;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getDateRace() {
        return dateRace;
    }

    public void setDateRace(String dateRace) {
        this.dateRace = dateRace;
    }

    public String getGroundType() {
        return groundType;
    }

    public void setGroundType(String groundType) {
        this.groundType = groundType;
    }

    public int getCountHorses() {
        return countHorses;
    }

    public void setCountHorses(int countHorses) {
        this.countHorses = countHorses;
    }

    public int getTimeHorse() {
        return timeHorse;
    }

    public void setTimeHorse(int timeHorse) {
        this.timeHorse = timeHorse;
    }

    public String getSpHorse() {
        return spHorse;
    }

    public void setSpHorse(String spHorse) {
        this.spHorse = spHorse;
    }

    public int getIdHorse() {
        return idHorse;
    }

    public void setIdHorse(int idHorse) {
        this.idHorse = idHorse;
    }

    public String getNameHorse() {
        return nameHorse;
    }

    public void setNameHorse(String nameHorse) {
        this.nameHorse = nameHorse;
    }

    public int getAgeHorse() {
        return ageHorse;
    }

    public void setAgeHorse(int ageHorse) {
        this.ageHorse = ageHorse;
    }

    public int getWeightHorse() {
        return weightHorse;
    }

    public void setWeightHorse(int weightHorse) {
        this.weightHorse = weightHorse;
    }

    public int getRatingHorse() {
        return ratingHorse;
    }

    public void setRatingHorse(int ratingHorse) {
        this.ratingHorse = ratingHorse;
    }
}
