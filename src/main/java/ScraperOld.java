import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Alex on 12.12.2016.
 */
public class ScraperOld {
    public static void main(String[] args) throws Exception {

        String[] mas = {
                "ctl00_ContentPlaceHolder1_genCtl7",        //1 Район
                "ctl00_ContentPlaceHolder1_genCtl47",       //2 Объект (квартира, комната)
                "ctl00_ContentPlaceHolder1_genCtl77",       //3 Количество комнат (в квартире)
                "ctl00_ContentPlaceHolder1_genCtl37",       //4 Тип
                "ctl00_ContentPlaceHolder1_genCtl76",       //5 Материал
                "ctl00_ContentPlaceHolder1_genCtl48",       //6 Этаж
                "ctl00_ContentPlaceHolder1_genCtl50",       //7 Этажность
                "ctl00_ContentPlaceHolder1_genCtl51",       //8 Площадь общая
                "ctl00_ContentPlaceHolder1_genCtl56"        //9 Цена
        };

        HashMap<String,Integer> rayon = new HashMap<String, Integer>();
        rayon.put("Орджоникидзевский",1);
        rayon.put("Свердловский",2);
        rayon.put("Ленинский",3);
        rayon.put("Дзержинский",4);
        rayon.put("Индустриальный",5);
        rayon.put("Кировский",6);
        rayon.put("Мотовилихинский",7);

        rayon.put("комната",1);
        rayon.put("две комнаты",2);
        rayon.put("1 - комнатная",3);
        rayon.put("2-х комнатная",4);
        rayon.put("3-х комнатная",5);
        rayon.put("4-х комн. и более",6);

        rayon.put("ХР",1);
        rayon.put("БР",2);
        rayon.put("УП",3);
        rayon.put("СП",4);
        rayon.put("МС",5);
        rayon.put("ИП",6);
        rayon.put("ПГ",7);
        rayon.put("ЛП",8);
        rayon.put("СТ",9);
        rayon.put("МГ",10);
        rayon.put("общежитие",11);
        rayon.put("старый фонд",12);

        rayon.put("панельный",1);
        rayon.put("монолитно-каркасный",2);
        rayon.put("кирпичный",3);
        rayon.put("шлакоблочный",3);

//        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
//        dataSource.setDriver(new org.postgresql.Driver());
//        dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
//        dataSource.setUsername("postgres");
//        dataSource.setPassword("postgres");
//        NamedParameterJdbcTemplate namedTemplate = new NamedParameterJdbcTemplate(dataSource);
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
//        jdbcTemplate.query("select * from matches", new ResultSetExtractor<Object>() {
//            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
//                while (rs.next()) {
//                    System.out.println(rs.getInt("id"));
//                }
//                return null;
//            }
//        });
//        File input = new File("/tmp/input.html");
//        Document doc = Jsoup.connect("http://www.rezon-realty.ru/EntityDescription.aspx?ItemID=281765").userAgent("Chrome").get();
//        Document doc = Jsoup.parse(new File("D:\\Other\\1.html"), "UTF-8", "http://example.com/");
//        Element e1 = doc.body().select(".covMainBoxContent").first();
//        Elements elements = e1.child(0).children().select("[style^=width:606px;height:22px;]");
//        for (Element element : elements) {
//            System.out.println(1);
//        }
//        System.out.println(1);

        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("1");
        int rn = 0;
//        for (int i = 1; i < 19; i++) {
//            for (int j = 0; j < 24; j++) {
//                Document doc = Jsoup.connect(Jsoup.parse(new File("D:\\Other\\html\\"+i+".html"), "UTF-8", "http://example.com/").select(".red11").get(j).attr("href")).userAgent("Chrome").get();
//                Row row = sheet.createRow(rn++);
//                for (int k = 0; k < mas.length; k++) {
//                    //System.out.println(doc.select("#"+mas[k]).html());
//                    row.createCell(k).setCellValue(doc.select("#"+mas[k]).html());
//                }
//            }
//        }        for (int i = 1; i < 19; i++) {
        int k = 0;
        for (int i = 0; i < 432; i++) {
            try {
                Document doc = Jsoup.parse(new File("D:\\Other\\test\\"+i+".html"), "UTF-8", "http://example.com/");
                Row row = sheet.createRow(rn++);
                Float area = null;
                for (k = 0; k < mas.length; k++) {
                    //System.out.println(doc.select("#"+mas[k]).html());
                    String value = doc.select("#"+mas[k]).html();
                    Integer val = 0;
                    Float price = null;
                    switch (k) {
                        case 0:
                        case 1:
                        case 3:
                        case 4: {
                            val = rayon.get(value);
                            row.createCell(k).setCellValue(val);
                            break;
                        }
                        case 8: {
                            price = Float.valueOf(value) / area;
                            String form = String.format(Locale.ENGLISH,"%.2f",price);
                            if (form.charAt(form.length()-1)=='0') {
                                form = form.substring(0,form.length()-1);
                            }
                            if (form.charAt(form.length()-1)=='0') {
                                form = form.substring(0,form.length()-2);
                            }
                            row.createCell(k).setCellValue(form);
                            break;
                        }
                        default: {
                            row.createCell(k).setCellValue(value);
                        }
                    }
                    if (k==7) {
                        area = Float.valueOf(value);
                    }
                    //row.createCell(k).setCellValue(val);
                }
            } catch (Exception e) {
                System.out.println(i + " " + k);
                e.printStackTrace();
            }

        }


        FileOutputStream fileOut = new FileOutputStream("D:\\Other\\3.xlsx");
        wb.write(fileOut);
        fileOut.close();
    }
}
