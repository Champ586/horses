import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ilyushchenko on 09.01.2017.
 */
public class PageParserNew {

    public static List<Race> races = new ArrayList<Race>();

    public static int count = 0;

    public static List<String> typesOfGround = new ArrayList<String>();

    public static void main(String[] args) {
        try {
            LocalDate curDate = LocalDate.of(2017, 5, 31);
            LocalDate endDate = LocalDate.of(2017, 5, 30);
            while (curDate.isAfter(endDate)) {
                try {
                    parseDay(curDate);
                } catch (Exception e) {
                    //System.out.println("Ошибка при парсинге " + curDate.format(DateTimeFormatter.ofPattern("dd-MMM-uuuu", Locale.ENGLISH)));
                    e.printStackTrace();
                }
                curDate = curDate.minusDays(1);
            }
        } catch (Exception mue) {
            mue.printStackTrace();
        }
    }

    private static void parseDay(LocalDate day) throws Exception {
        Document docDay = getDoc("http://www.attheraces.com/racecards/"+day.format(DateTimeFormatter.ofPattern("dd-MMM-uuuu", Locale.ENGLISH)));
        //System.out.println("http://www.attheraces.com/racecards/"+day.format(DateTimeFormatter.ofPattern("dd-MMM-uuuu", Locale.ENGLISH)));
        Elements panels = docDay.select(".panel.uk");
        for (int i = 0; i < panels.size(); i++) {
            Element panel = panels.get(i);


            String weather = null;
            Elements weatherSpans = panel.select(".meeting-information").select("span");
            for (Element span : weatherSpans) {
                if (span.text().contains("Weather")) {
                    weather = span.text().split(":")[1].substring(1);
                }
            }
            if (weather==null) {
                return;
            }
            //System.out.println("weather:" + weather);

            Elements races = panel.select(".finished.uk");
            for (int j=0; j<races.size(); j++) {
                try {
                    Element element = races.get(j);
                    //System.out.println(element.select("a.meeting").attr("href"));
                    parseRace("http://www.attheraces.com"+element.select("a.meeting").attr("href"), weather);
                } catch (Exception e) {
                    //System.out.println("Ошибка при парсинге гонки");
                    e.printStackTrace();
                }
            }
        }

    }

    private static void parseRace(String href, String weather) throws Exception {
        String[] raceData = new String[6];
        Document race = getDoc(href);
        System.out.println("Гонка:" + href);
//        Integer raceId = Integer.parseInt(race.html().substring(race.html().indexOf("RaceID:")+7, race.html().indexOf("PaRaceID")-10));
        //Integer distance = getYards(race.select(".distance").select("span").text());
        //System.out.println("Distance:" + distance);
        LocalDate raceDate = null;
        try {
            raceDate = LocalDate.parse(race.select("span.date").text(), DateTimeFormatter.ofPattern("dd MMM uuuu", Locale.ENGLISH));
        } catch (Exception e) {
            raceDate = LocalDate.parse(race.select("span.date").text(), DateTimeFormatter.ofPattern("d MMM uuuu", Locale.ENGLISH));
        }
        //String going = race.select(".race-head__distance-and-going").select("p").get(1).text();
        //System.out.println("Type:" + going);
        //Integer winningTime = getMilliseconds(race.select(".racecard-results-footer.panel-footer.panel-inner").select("p").get(1).text().split(":")[1].substring(1));
        Elements rows = race.select(".table-racecard-results").select("tbody").select("tr:not(.non-runner)");
        //System.out.println("Количество в забеге:" + rows.size());
        //Integer horseTime = winningTime;
        //System.out.println("Время лошади:" + horseTime);
        for (int i = 0; i < rows.size(); i++) {
            Element horse = rows.get(i);
            Document horsePage = getDoc("http://www.attheraces.com"+horse.select(".name.summary.form-link.horse-form-link").attr("href"));
            System.out.println("http://www.attheraces.com"+horse.select(".name.summary.form-link.horse-form-link").attr("href"));
            Elements hRows = horsePage.select("#horse-form-full").select("tbody").select("tr:not(.divide.hidden-with-filters)");
            int k = 0;
            for (int j = 0; j < hRows.size(); j++) {
                Element row = hRows.get(j);
                String[] strDateMas = row.select("a").get(0).text().split(" ");
                String strDate = strDateMas[0] + " " + strDateMas[1] + " " + strDateMas[2];
                LocalDate date = LocalDate.parse(strDate, DateTimeFormatter.ofPattern("dd MMM uu", Locale.ENGLISH));
                if (date.equals(raceDate)) {
                    k = j;
                }
            }
            if ((hRows.size()-k)<7) {
                continue;
            }

            String[][] horseRacesMas = new String[7][];
            int p = 0;


            for (int j = k; j < k+7; j++) {

                Element row = hRows.get(j);

                String[] strDateMas = row.select("td a").get(0).text().split(" ");
                Integer daysSinceLastRide = 0;
                if (strDateMas.length==4) {
                    daysSinceLastRide = Integer.valueOf(strDateMas[3].substring(1, strDateMas[3].length()-1));
                }
                Race curRace = new Race();
                //System.out.println("Дней с последней гонки: "+daysSinceLastRide);
                curRace.setDaysFromRace(daysSinceLastRide);
                /*
                String raceClass = row.attr("data-raceclass");
                if (raceClass.length()==0) {
                    break;
                }
                */
                String raceType = row.attr("data-racetype");
                //System.out.println("Тип гонки: "+raceType);
                curRace.setRaceType(raceType);
                String positionStr = row.select("td").get(3).select("span").get(0).text().split("/")[0];
                Integer position = null;
                if (positionStr.equals("Won")) {
                    position = 1;
                } else {
                    try {
                        position = Integer.valueOf(positionStr);
                    } catch (Exception e) {
                        races.clear();
                        break;
                    }
                }
                curRace.setPlace(position);
                String horseRaceHref = row.select("td").get(0).select("a").attr("href");
                //System.out.println("Ссылка на гонку: " + "http://www.attheraces.com/form-popup/race" + horseRaceHref.substring(horseRaceHref.indexOf("formracecard")+12));
                curRace.setHrefRace("http://www.attheraces.com/form-popup/race" + horseRaceHref.substring(horseRaceHref.indexOf("formracecard")+12));
                //System.out.println(positionStr + " " + position);
                parseRaceBefore("http://www.attheraces.com/form-popup/race" + horseRaceHref.substring(horseRaceHref.indexOf("formracecard")+12),position,curRace);
                //Integer horseId = Integer.parseInt(horse.select(".horse-tracker-link").attr("data-horseid"));
                //System.out.println("Идентификатор:"  + horseId);
                //curRace.setIdHorse(horseId);
                //String horseName = horse.attr("data-horsename");
                //System.out.println("Имя лошади: " + horseName);
                //curRace.setNameHorse(horseName);
                //Integer age = Integer.parseInt(horse.select("span.age").text());
                //System.out.println("Возраст лошади: " + age);
                //curRace.setAgeHorse(age);
                //String[] weightMas = horse.select("span.weight").text().split("-");
                //Integer weight = Integer.parseInt(weightMas[0])*14 + Integer.parseInt(weightMas[1].split(" ")[0]);
                //System.out.println("Вес лошади: " + weight);
                //curRace.setWeightHorse(weight);
                // ratingStr = horse.select(".official-rating").select("span").get(0).text();
                //Integer officialRating = ratingStr.equals("-")?null:Integer.parseInt(ratingStr);
                //System.out.println("Рейтинг лошади: " + officialRating);
                //try{
                //    curRace.setRatingHorse(officialRating);
                //}
                //catch (NumberFormatException e){
                //    curRace.setRatingHorse(0);
                //}
                races.add(curRace);

            }
            if (races.size() > 5) {
                writeToExcel(races);
                System.out.println(count);
            }


            //System.out.println("Итог:" + position);

            //String breakAway = horse.select("td.distance").text();
            //horseTime += parseBreakAway(breakAway);
        }
    }

    private static void parseRaceBefore(String href, Integer position, Race curRace) throws Exception {
        Document docDay = getDoc(href);
        Integer distance = getYards(docDay.select(".distance").select("span").text());
        //System.out.println("Distance:" + distance);
        curRace.setDistance(distance);
        LocalDate raceDate = null;
        try {
            raceDate = LocalDate.parse(docDay.select("span.date").text(), DateTimeFormatter.ofPattern("dd MMM uuuu", Locale.ENGLISH));
        } catch (Exception e) {
            raceDate = LocalDate.parse(docDay.select("span.date").text(), DateTimeFormatter.ofPattern("d MMM uuuu", Locale.ENGLISH));
        }
        //System.out.println("Дата: " + raceDate);
        curRace.setDateRace(raceDate.toString());
        String going = docDay.select(".race-head__distance-and-going").select("p").get(1).text();
        //System.out.println("Type: " + going);
        curRace.setGroundType(going);
        Integer winningTime = getMilliseconds(docDay.select(".racecard-results-footer.panel-footer.panel-inner").select("p").get(1).text().split(":")[1].substring(1));
        Elements rows = docDay.select(".table-racecard-results").select("tbody").select("tr:not(.non-runner)");
        //System.out.println("Количество в забеге:" + rows.size());
        curRace.setCountHorses(rows.size());
        Integer horseTime = winningTime;
        String sp = rows.get(0).select("td.starting-price").select("span").text();
        for (int i = 1; i < position; i++) {
            String breakAway = docDay.select("td.distance").get(i).text();
            horseTime += parseBreakAway(breakAway);
            sp = rows.get(i).select("td.starting-price").select("span").text();
        }
        Integer age = Integer.parseInt(rows.get(position-1).select("td.age-weight").select("span.age").text());
        curRace.setAgeHorse(age);
        String[] weightMas = rows.get(position-1).select("td.age-weight").select("span.weight").text().split("-");
        Integer weight = Integer.parseInt(weightMas[0])*14 + Integer.parseInt(weightMas[1].split(" ")[0]);
        curRace.setWeightHorse(weight);
        String ratingStr = rows.get(position-1).select("td.official-rating").select("span.icon-text-steel").get(0).text();
        Integer officialRating = ratingStr.equals("-")?null:Integer.parseInt(ratingStr);
        //System.out.println("Рейтинг лошади: " + officialRating);
        try{
            curRace.setRatingHorse(officialRating);
        }
        catch (NullPointerException e){
            curRace.setRatingHorse(0);
        }
        //System.out.println("Время лошади:" + horseTime);
        //System.out.println("Коэффициент: " + sp);
        //System.out.println("Коэффициент: " + parseSP(sp));
        curRace.setSpHorse(sp);
        curRace.setTimeHorse(horseTime);
    }

    private static Document getDoc(String url) throws Exception {
        int tries = 5;
        while (tries>0) {
            try {
                return Jsoup.connect(url).timeout(0).userAgent("Chrome").get();
            } catch (Exception e) {
                tries--;
                //System.out.println(tries);
                if (tries==0) {
                    throw e;
                }
            }
        }
        return null;
    }

    private static Integer getYards(String distance) {
        Integer yards = 0;
        String[] distMas = distance.split(" ");
        for (String dist : distMas) {
            if (dist.contains("y")) {
                yards += Integer.parseInt(dist.replace("y",""));
            }
            if (dist.contains("f")) {
                yards += Integer.parseInt(dist.replace("f","")) * 220;
            }
            if (dist.contains("m")) {
                yards += Integer.parseInt(dist.replace("m","")) * 8 * 220;
            }
        }
        return yards;
    }

    private static Integer getMilliseconds(String winningTime) {
        Integer milliSeconds = 0;
        String[] timeMas = winningTime.split(" ");
        for (String time : timeMas) {
            if (time.contains("m")) {
                milliSeconds += Integer.parseInt(time.replace("m","")) * 60 * 1000;
            }
            if (time.contains("s")) {
                String[] secondsMas = time.replace("s","").split("\\.");
                milliSeconds += Integer.parseInt(secondsMas[0].length()>0?secondsMas[0]:"0") * 1000 + Integer.parseInt(secondsMas[1]) * 10;
            }
        }
        return milliSeconds;
    }

    private static Integer parseBreakAway(String breakAway) {
        if (breakAway.length()==0) {
            return 0;
        }
        Integer milliSeconds = 0;
        switch (breakAway) {
            case "shd": {
                milliSeconds += 1;
                break;
            }
            case "nse": {
                milliSeconds += 5;
                break;
            }
            case "hd": {
                milliSeconds += 15;
                break;
            }
            case "dh": {
                milliSeconds += 20;
                break;
            }
            case "nk": {
                milliSeconds += 35;
                break;
            }
            default: {
                if (breakAway.contains("¼")) {
                    breakAway = breakAway.replace("¼","");
                    milliSeconds += 45;
                }
                if (breakAway.contains("½")) {
                    breakAway = breakAway.replace("½","");
                    milliSeconds += 90;
                }
                if (breakAway.contains("¾")) {
                    breakAway = breakAway.replace("¾","");
                    milliSeconds += 135;
                }
                if (breakAway.length()>0) {
                    milliSeconds += Integer.parseInt(breakAway) * 180;
                }
            }
        }
        return milliSeconds;
    }
    private static Double parseSP(String sp) {
        sp = sp.replaceAll("\\s|[A-Za-zА]","");
        double result = 0;
        try {
            result = Double.parseDouble(sp.substring(0, sp.indexOf("/"))) / Double.parseDouble(sp.substring(sp.indexOf("/") + 1));
        }
        catch (StringIndexOutOfBoundsException e){
            System.out.println(sp);
        }
        if (result < 1){
            result = result + 1;
        }
        return result;
    }

    private static void writeToExcel (List<Race> races1) throws IOException, InvalidFormatException {
         //FileOutputStream out = new FileOutputStream("races.xls");
         //XSSFWorkbook wb = new XSSFWorkbook();// create a new workbook
         //Sheet sheet = wb.getSheetAt(0);
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = null;
        if (count==0){
            sheet = wb.createSheet("new sheet");
        }
        else{
            wb = WorkbookFactory.create(new FileInputStream("races.xls"));
            sheet = wb.getSheetAt(0);
        }
        Row row = sheet.getRow(count);
        if (row==null){
            row = sheet.createRow(count);
        }
        int tables = 0;
        for (int i = 0 ; i < races1.size(); i++){
            Race curRace = races1.get(i);
            row.createCell(tables).setCellValue(curRace.getHrefRace());
            tables++;
            //row.createCell(tables).setCellValue(curRace.getNameHorse());
            row.createCell(tables).setCellValue(curRace.getDaysFromRace());
            tables++;
            row.createCell(tables).setCellValue(curRace.getDistance());
            tables++;
            row.createCell(tables).setCellValue(curRace.getTimeHorse());
            tables++;
            row.createCell(tables).setCellValue(parseSP(curRace.getSpHorse()));
            tables++;
            row.createCell(tables).setCellValue(curRace.getAgeHorse());
            tables++;
            row.createCell(tables).setCellValue(curRace.getWeightHorse());
            tables++;
            row.createCell(tables).setCellValue(curRace.getRatingHorse());
            tables++;
            //row.createCell(tables).setCellValue(curRace.getDateRace());
            //tables++;
            if (curRace.getRaceType().contains("handicap")){
                row.createCell(tables).setCellValue(1);
            }
            else {
                row.createCell(tables).setCellValue(0);
            }
            tables++;

            row.createCell(tables).setCellValue(curRace.getGroundType());
            tables++;
            if (typesOfGround.size() == 0){
                typesOfGround.add(curRace.getGroundType());
                row.createCell(tables).setCellValue(1);
            }
            else {
                boolean checktype = false;
                for (int j = 0; j<typesOfGround.size(); j++){
                    if (typesOfGround.get(j).equals(curRace.getGroundType())){
                        checktype = true;
                        row.createCell(tables).setCellValue(j+1);
                        break;
                    }
                }
                if (!checktype){
                    typesOfGround.add(curRace.getGroundType());
                    row.createCell(tables).setCellValue(typesOfGround.size()+1);
                }

            }
            tables++;
            int place = curRace.getPlace();
            int counthorses = curRace.getCountHorses();
            if (place==1){
                row.createCell(tables).setCellValue(1);
                tables++;
            }
            else if (place==counthorses){
                row.createCell(tables).setCellValue(0);
                tables++;
            }
            else {
                double partOfCount = 1-(double)place/counthorses;
                row.createCell(tables).setCellValue(partOfCount);
                tables++;
            }
        }
        count++;
        races.clear();
        FileOutputStream fileOut = new FileOutputStream("races.xls");
        wb.write(fileOut);
        fileOut.close();
        //wb.write(out);
        //out.close();
    }

}
