import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.net.SocketTimeoutException;

/**
 * Created by Alexey on 10.01.2017.
 */
public class DownloadFootballDatabase {
    public static void main(String[] args) throws IOException, InterruptedException {

        Connection.Response loginForm = Jsoup.connect("http://www.footballdatabase.eu")
                .data("crealog", "1")
                .data("connect.x", "18")
                .data("connect.y", "12")
                //.proxy("139.59.136.45", 8080)
                .method(Connection.Method.POST)
                .execute();

        String tour = "";
        int match = 0;

        for (int i = 20; i < 38; i++) {
            if (i < 9) {
                tour = "";
                tour = tour + "0" + (i + 1);
            } else {
                tour = "";
                tour = tour + (i + 1);

            }
            //Thread.sleep(300 + (int) (Math.random() * 1000));
            //String tour_page = cur_page("http://www.footballdatabase.eu/football.competition.primera-division.espagne.2000-2001."+tour +".151.en.html",cur_proxy());
            Document doc = Jsoup.connect("http://www.footballdatabase.eu/football.competition.liga-bbva.espagne.2015-2016." + tour + ".4509.en.html").cookies(loginForm.cookies()).get();
            //File file = new File("d:\\SpanishDatabase\\2015-2016\\"+tour+".html");
            //BufferedWriter htmlWriter =
            //        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            //htmlWriter.write(doc.toString());
            System.out.println(tour);


            int count = doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").size();
            for (int j = 2; j < count; j++) {
                if (doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").get(j).hasClass("stylemneutre")) {
                    //String team1 = doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").get(j).select("td").get(5).select("a").text();
                    // String goalteam1 = doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").get(j).select("td").get(6).text();
                    String statlink = doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").get(j).select("td").get(6).attr("onclick").substring(17);
                    int goalteam1 = (int) Double.parseDouble(doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").get(j).select("td").get(6).text());
                    int goalteam2 = (int) Double.parseDouble(doc.select("table[class=fondsoustitrembleu488]").first().select("tbody").select("tr").get(j).select("td").get(7).text());
                    int statlinklength = statlink.length();
                    statlink = statlink.substring(0, statlinklength - 1);
                    String matchlink = statlink;
                    statlink = "http://www.footballdatabase.eu/" + statlink;
                    System.out.println(statlink);
                   // Thread.sleep(400 + (int) (Math.random() * 1300));
                    // String match_page = cur_page(statlink,cur_proxy());
                    //if (!checkfile("d:\\SpanishDatabase\\2015-2016\\",matchlink)){
                    Document doc2 = Jsoup.connect(statlink).cookies(loginForm.cookies()).timeout(10000).get();
                    //File file2 = new File("d:\\SpanishDatabase\\2015-2016\\"+matchlink);
                    //BufferedWriter htmlWriter2 =
                     //       new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file2), "UTF-8"));
                   // htmlWriter2.write(doc2.toString());

                    int playersfirstteam = doc2.select("table[class=tabmodulebleu488").get(1)
                            .select("tbody").first().select("tr").get(0).select("td").get(0)
                            .select("div").first().select("table").first().select("tbody").first().select("tr").get(0)
                            .select("td").get(0).select("table").first().select("tbody").first().select("tr").size();

                    for (int k = 0; k<playersfirstteam;k++){
                        if (k!=11){
                            //String player1 = doc2.select("table[class=tabmodulebleu488").get(1)
                             //       .select("tbody").first().select("tr").get(0).select("td").get(0)
                             //       .select("div").first().select("table").first().select("tbody").first().select("tr").get(0)
                             //       .select("td").get(0).select("table").first().select("tbody").first().select("tr").get(k)
                            //        .select("td").get(3).select("a").get(0).select("b").text();
                           // System.out.println(player1);
                            String player1link = doc2.select("table[class=tabmodulebleu488").get(1)
                                    .select("tbody").first().select("tr").get(0).select("td").get(0)
                                    .select("div").first().select("table").first().select("tbody").first().select("tr").get(0)
                                    .select("td").get(0).select("table").first().select("tbody").first().select("tr").get(k)
                                    .select("td").get(3).select("a").attr("href");
                            //System.out.println(player1 + " " + player1link);
                            String curlink = player1link;
                            if ((!checkfile("d:\\SpanishDatabase\\Players\\",curlink))&&(!player1link.equals(""))) {
                                player1link = "http://www.footballdatabase.eu/" + player1link;
                                Document doc3 = Jsoup.connect(player1link).cookies(loginForm.cookies()).timeout(10000).get();
                                File file3 = new File("d:\\SpanishDatabase\\Players\\" + curlink);
                                BufferedWriter htmlWriter3 =
                                        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file3), "UTF-8"));
                                htmlWriter3.write(doc3.toString());

                            }
                        }
                    }

                    int playersfirstteam2 = doc2.select("table[class=tabmodulebleu488").get(1)
                            .select("tbody").first().select("tr").get(0).select("td").get(0)
                            .select("div").first().select("table").first().select("tbody").first().select("tr").get(0)
                            .select("table").get(1).select("tbody").first().select("tr").size();

                    for (int k = 0; k<playersfirstteam2;k++){
                        if (k!=11){
                            //String player1 = doc2.select("table[class=tabmodulebleu488").get(1)
                            //       .select("tbody").first().select("tr").get(0).select("td").get(0)
                            //       .select("div").first().select("table").first().select("tbody").first().select("tr").get(0)
                            //       .select("td").get(0).select("table").first().select("tbody").first().select("tr").get(k)
                            //        .select("td").get(3).select("a").get(0).select("b").text();
                            // System.out.println(player1);
                            String player2link = doc2.select("table[class=tabmodulebleu488").get(1)
                                    .select("tbody").first().select("tr").get(0).select("td").get(0)
                                    .select("div").first().select("table").first().select("tbody").first().select("tr").get(0)
                                    .select("table").get(1).select("tbody").first().select("tr").get(k)
                                    .select("td").get(3).select("a").attr("href");
                            //System.out.println(player1 + " " + player1link);
                            String curlink2 = player2link;
                            if ((!checkfile("d:\\SpanishDatabase\\Players\\",curlink2))&&(!player2link.equals(""))) {
                                player2link = "http://www.footballdatabase.eu/" + player2link;
                                Document doc4 = Jsoup.connect(player2link).cookies(loginForm.cookies()).timeout(10000).get();
                                File file4 = new File("d:\\SpanishDatabase\\Players\\" + curlink2);
                                BufferedWriter htmlWriter3 =
                                        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file4), "UTF-8"));
                                htmlWriter3.write(doc4.toString());

                            }
                        }
                    }







/*тренеры
                    String coach1 = doc2.select("table[class=tabmoduleclairbleu488").first()
                            .select("tbody").first().select("tr").get(1).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(2).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(0).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").last().select("td").first()
                            .select("div").get(1).select("a").text();
                    String linkcoach1 = doc2.select("table[class=tabmoduleclairbleu488").first()
                            .select("tbody").first().select("tr").get(1).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(2).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(0).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").last().select("td").first()
                            .select("div").get(1).select("a").attr("href");
                    String coachlink1 = linkcoach1;
                    //System.out.println(coachlink1);


                    if ((!checkfile("d:\\SpanishDatabase\\Coaches\\",coachlink1))&&(!linkcoach1.equals(""))) {
                        linkcoach1 = "http://www.footballdatabase.eu/" + linkcoach1;
                       // Thread.sleep(200 + (int) (Math.random() * 1400));
                        Document doc3 = Jsoup.connect(linkcoach1).cookies(loginForm.cookies()).get();
                        File file3 = new File("d:\\SpanishDatabase\\Coaches\\" + coachlink1);
                        BufferedWriter htmlWriter3 =
                                new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file3), "UTF-8"));
                        htmlWriter3.write(doc3.toString());

                    }


                    String coach2 = doc2.select("table[class=tabmoduleclairbleu488").first()
                            .select("tbody").first().select("tr").get(1).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(2).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(0).select("td").get(7)
                            .select("table").first().select("tbody").first().select("tr").last().select("td").first()
                            .select("div").get(1).select("a").text();;
                    String linkcoach2 = doc2.select("table[class=tabmoduleclairbleu488").first()
                            .select("tbody").first().select("tr").get(1).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(2).select("td").get(0)
                            .select("table").first().select("tbody").first().select("tr").get(0).select("td").get(7)
                            .select("table").first().select("tbody").first().select("tr").last().select("td").first()
                            .select("div").get(1).select("a").attr("href");
                    String coachlink2 = linkcoach2;
                    if ((!checkfile("d:\\SpanishDatabase\\Coaches\\",coachlink2))&&(!linkcoach2.equals(""))) {
                        linkcoach2 = "http://www.footballdatabase.eu/" + linkcoach2;
                        //Thread.sleep(200 + (int) (Math.random() * 1300));
                        Document doc4 = Jsoup.connect(linkcoach2).cookies(loginForm.cookies()).get();
                        File file4 = new File("d:\\SpanishDatabase\\Coaches\\" + coachlink2);
                        BufferedWriter htmlWriter4 =
                                new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file4), "UTF-8"));
                        htmlWriter4.write(doc4.toString());

                    }
*/

                  //  match++;
                 }
              //  }


            }
        }

    }

    public static boolean checkfile(String CurPath, String filename) throws IOException {
        boolean result = false;
        File []fList;
        File F = new File(CurPath);
        fList = F.listFiles();
        for(int i=0; i<fList.length; i++)
        {
            if(fList[i].isFile()) {
                //System.out.println(fList[i].getName());
                if (fList[i].getName().equals(filename)){
                    result=true;
                    i=fList.length+1;
                }

            }

        }

        return result;
    }


}



