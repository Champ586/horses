CREATE TABLE public.races (
  race_id INTEGER NOT NULL,
  distance INTEGER,
  going VARCHAR,
  date DATE,
  weather VARCHAR,
  CONSTRAINT races_pkey PRIMARY KEY(race_id)
)
WITH (oids = false);

COMMENT ON COLUMN public.races.distance
IS 'Дистанция в йардах';

COMMENT ON COLUMN public.races.going
IS 'Условия';

CREATE TABLE public.participants (
  race_id INTEGER,
  pos INTEGER,
  horse_name VARCHAR,
  horse_id INTEGER NOT NULL,
  weight INTEGER,
  rating INTEGER,
  age INTEGER,
  breakaway VARCHAR
)
WITH (oids = false);